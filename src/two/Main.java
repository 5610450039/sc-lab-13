package two;

public class Main {
	public static void main(String[] args){
		QueueC q = new QueueC();
		Consumer con = new Consumer(q);
		Producer pro = new Producer(q);
		
		Thread t1 = new Thread(con);
		Thread t2 = new Thread(pro);
		
		t1.start();
		t2.start();
	}

}
