package two;

import java.util.Date;

public class Consumer implements Runnable {
	QueueC q;
	
	public Consumer (QueueC q){
		this.q = q;
	}
	public void run(){
		for(int i = 0;i<100;i++){
			try {
				dequeue();
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	public void dequeue(){
		String text = q.dequeue();
		System.out.println(text);
	}

}
