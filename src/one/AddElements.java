package one;
import java.util.ArrayList;
import java.util.LinkedList;


public class AddElements implements Runnable {
	private static final int DELAY = 150; 
	private LinkedList<Integer> lList;
	private int count;
	public AddElements(LinkedList<Integer> l,int i){
		lList = l;
		count = i;
	}
	
	@Override
	public void run() {
		 try
	      {
	         for (int i = 1; i <= count; i++)
	         {
	        	 lList.add(i);
	            Thread.sleep(DELAY);
	            System.out.println(lList);
	         }
	      }
	      catch (InterruptedException exception) {}
	   }
}
	
	
