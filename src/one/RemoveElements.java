package one;
import java.util.ArrayList;
import java.util.LinkedList;


public class RemoveElements implements Runnable {
	private static final int DELAY = 150; 
	private LinkedList<Integer> lList;
	private int count;
	public RemoveElements(LinkedList<Integer> l,int i){
		lList = l;
		count = i;
	}
	
	@Override
	public void run() {
		 try
	      {
	         for (int i = 1; i <= count; i++)
	         {
	        	lList.remove(lList.size()-1);
	            Thread.sleep(DELAY);
	            System.out.println(lList);
	         }
	      }
	      catch (InterruptedException exception) {}
	   }
}
