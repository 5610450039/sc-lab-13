package one;
import java.util.ArrayList;
import java.util.LinkedList;

public class ThreadRunner {
	public static void main(String[] args){
		LinkedList<Integer> lList = new LinkedList<Integer>();
		AddElements addE = new AddElements(lList,3);
		RemoveElements removeE = new RemoveElements(lList,3);
		
		Thread t1 = new Thread(addE);
		Thread t2 = new Thread(removeE);
		
		t1.run();
		t2.run();
		
		
		
	}
}
